'use strict';

var CITY_MAPPER_GREEN = '#38ab2e'
var CITY_MAPPER_PURPLE = '#8a5c95'
var CITY_MAPPER_BLUE = '#407394'

var React = require('react-native')

var {
  Text,
  TouchableHighlight,
  LinkingIOS,
  StyleSheet
} = React


var CityMapper = React.createClass({
  propTypes: {
    startLatitude: React.PropTypes.number,
    startLongitude: React.PropTypes.number,
    endLatitude: React.PropTypes.number.isRequired,
    endLongitude: React.PropTypes.number.isRequired,
    endAddress: React.PropTypes.string,
    endName: React.PropTypes.string
  },

  getInitialState: function() {
    return {
      installed: false,
      url: this._buildProtocolString()
    }
  },

  componentWillMount: function () {
    var url = this.state.url
    LinkingIOS.canOpenURL(url, (supported) => {
      if (!supported) {
        this.setState({installed: false})
      } else {
        this.setState({installed: true})
      }
    })
  },

  _buildProtocolString: function() {
    var url = 'citymapper://directions?startcoord='
    url = url + this.props.startLatitude
    url = url + ','
    url = url + this.props.startLongitude
    url = url + '&startname=You'
    url = url + '&endcoord='
    url = url + this.props.endLatitude
    url = url + ','
    url = url + this.props.endLongitude
    url = url + '&endname='
    url = url + encodeURIComponent(this.props.endName)
    url = url + '&endaddress='
    url = url + encodeURIComponent(this.props.endAddress)
    return url
  },

  _openInCityMapper: function () {
    return LinkingIOS.openURL(this.state.url)
  },

  _renderButton: function() {
    return (
      <TouchableHighlight
        style={styles.button}
        underlayColor={CITY_MAPPER_BLUE}
        onPress={this._openInCityMapper}
      >
        <Text style={styles.buttonText}>
          Open in Citymapper
        </Text>
      </TouchableHighlight>
    )
  },

  render: function() {
    if (this.state.installed) {
      return this._renderButton()
    } else {
      return (<Text />)
    }
  }
})

var styles = StyleSheet.create({
  button: {
    alignSelf: 'stretch',
    borderColor: CITY_MAPPER_PURPLE,
    borderRadius: 0,
    borderWidth: 2,
    flexDirection: 'row',
    height: 40,
    justifyContent: 'center',
    paddingBottom: 2
  },
  buttonText: {
    fontFamily: 'Montserrat-Bold',
    color: CITY_MAPPER_GREEN,
    fontSize: 18,
    alignSelf: 'center'
  }
})

module.exports = CityMapper

