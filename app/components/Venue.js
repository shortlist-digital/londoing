'use strict';

var React = require('react-native');

var {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  View,
  NavigatorIOS,
  Image,
  WebView,
  TouchableOpacity
} = React;

var {vw, vh, vmin, vmax} = require('react-native-viewport-units')
var LinearGradient = require('react-native-linear-gradient')

var Venue = React.createClass({

  _supplyProps: function() {
    this.props.onVenuePress(this.props)
  },

  render: function() {
    return (
      <TouchableOpacity onPress={this._supplyProps} activeOpacity={0.7}>
        <View>
          <View style={styles.container}>
            <View style={styles.contentContainer}>
              <Image
                style={styles.photo}
                source={{uri: this.props.image}}
              />
            </View>

            <LinearGradient colors={['rgba(0,0,0,0)', '#000']} style={styles.linearGradient}>
            </LinearGradient>

            <View style={styles.textContainer}>
              <Text style={styles.name}>
                {this.props.name}
              </Text>
              <Text style={styles.distance}>
                {this.props.distance} miles away
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  },
});

var styles = StyleSheet.create({
  label: {
    color: '#ffffff'
  },

  linearGradient: {
    backgroundColor: 'transparent',
    flex: 1,
    height: (60*vw),
    width: (100*vw),
    position: 'absolute',
    top: 0
  },

  distance: {
    fontFamily: 'Montserrat-Regular',
    color: '#ffffff',
    flex: 1
  },

  textContainer: {
    backgroundColor: 'transparent',
    bottom: 0,
    padding: 20,
    position: 'absolute',
    alignSelf: 'stretch'
  },

  name: {
    fontFamily: 'Montserrat-Bold',
    fontWeight: '100',
    marginBottom: 5,
    fontSize: 26,
    color: '#ffffff'
  },

  text: {
    fontFamily: 'Montserrat-Bold',
    color: '#333333',
    marginTop: 5,
    marginBottom: 5
  },

  container: {
    flex: 1,
    alignItems: 'stretch',
    position: 'relative',
    marginTop: 0
  },
  contentContainer: {
    height: (60*vw),
    width: (100*vw),
    flex: 1,
    alignItems: 'stretch'
  },
  photo: {
    flex: 1
  },
});

module.exports = Venue
