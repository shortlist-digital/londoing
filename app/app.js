'use strict';

var React = require('react-native')
var { Parse } = require('parse')
global.React = React
global.Parse = Parse

Parse.initialize('bunOXFHyuPkAsCG7PknSlUQt9bJaBTB9tL9nJtnU', 'tC6ojy8ivKhHbXkCY4QXbM7uxbPppmCQQ8CqjSe0')

var ParseReact = require('parse-react/dist/parse-react.js')
var {vw, vh, vmin, vmax} = require('react-native-viewport-units')

var NAVBAR_COLOR = '#5856d6'

var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  ScrollView,
  StatusBarIOS,
  Navigator,
  PixelRatio,
  TouchableOpacity
} = React


var ResultsScreen = require('./pages/ResultsScreen.ios.js')
var SearchScreen = require('./pages/SearchScreen.ios.js')
var VenueScreen = require('./pages/VenueScreen.ios.js')

var NavigationBarRouteMapper = {

  LeftButton: function(route, navigator, index, navState) {
    if (index === 0) {
      return null;
    }

    var previousRoute = navState.routeStack[index - 1];
    return (
      <TouchableOpacity
        onPress={() => navigator.pop()}>
        <View style={styles.navBarLeftButton}>
          <Text style={[styles.navBarText, styles.navBarButtonText]}>
            {previousRoute.title}
          </Text>
        </View>
      </TouchableOpacity>
    )
  },

  RightButton: function(route, navigator, index, navState) {
    return (<View />)
    // Ignoring for now
    return (
      <TouchableOpacity
        onPress={() => navigator.push(newRandomRoute())}>
        <View style={styles.navBarRightButton}>
          <Text style={[styles.navBarText, styles.navBarButtonText]}>
            Next
          </Text>
        </View>
      </TouchableOpacity>
    )
  },

  Title: function(route, navigator, index, navState) {
    return (
      <Text style={[styles.navBarText, styles.navBarTitleText]}>
        {route.title}
      </Text>
    )
  }
}



var Londoing = React.createClass({

  componentWillMount: function() {
    //StatusBarIOS.setStyle(StatusBarIOS.Style['lightContent'])
  },

  _renderScene: function(route, nav) {
    switch (route.id) {
      case 'venue':
        return (
          <VenueScreen
            name={route.id}
            navigator={nav}
            route={route}
            {...route.passProps}
          />
        )
      case 'results':
        return (
          <ResultsScreen
            name={route.id}
            navigator={nav}
            route={route}
            {...route.passProps}
          />
        )
      default:
        return (
          <SearchScreen
            name="search"
            route={route}
            navigator={nav}
          />
        )
    }
  },

  _renderScrollWrapper: function(scroll, nav) {
    return (
      <ScrollView style={styles.scene}>
        {this._renderScene(scroll, nav)}
      </ScrollView>
    )
  },

  _handleNewTransition: function(scene) {
    this.setState({
      currentScene: scene.id
    })
  },

  render: function() {
    return (
      <Navigator
        onWillFocus={this._handleNewTransition}
        navigationBar={
          <Navigator.NavigationBar
            routeMapper={NavigationBarRouteMapper}
            style={styles.navBar}
          />
        }
        style={styles.container}
        initialRoute={{title: 'Search', index: 0}}
        renderScene={this._renderScrollWrapper}
        configureScene={(route) => {
          if (route.sceneConfig) {
            return route.sceneConfig
          }
          return Navigator.SceneConfigs.FloatFromRight
        }}
      />
    )
  }
})

var styles = StyleSheet.create({
  messageText: {
    fontSize: 17,
    fontWeight: '500',
    padding: 15,
    marginTop: 50,
    marginLeft: 15,
  },
  button: {
    backgroundColor: 'white',
    padding: 15,
    borderBottomWidth: 1 / PixelRatio.get(),
    borderBottomColor: '#CDCDCD',
  },
  buttonText: {
    fontSize: 17,
    fontWeight: '500',
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    height: (100*vh)-60,
    width: 100*vw,
  },
  navBar: {
    backgroundColor: '#F7F7F7'
  },
  navBarText: {
    color: NAVBAR_COLOR,
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    marginVertical: 10,
  },
  navBarTitleText: {
    color: NAVBAR_COLOR,
    fontFamily: 'Montserrat-Bold',
    marginVertical: 9,
  },
  navBarLeftButton: {
    paddingLeft: 10,
  },
  navBarRightButton: {
    paddingRight: 10,
  },
  navBarButtonText: {
    color: NAVBAR_COLOR
  },
  scene: {
    flex: 1,
    backgroundColor: 'white',
    position: 'absolute',
    bottom: 0,
    height: (100*vh)-60,
    width: 100*vw,
    alignSelf: 'stretch'
  },
})


AppRegistry.registerComponent('Londoing', () => Londoing)

module.exports = Londoing
