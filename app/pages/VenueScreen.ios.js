'use strict';

var React = require('react-native')
var { Parse } = require('parse')
var ParseReact = require('parse-react/dist/parse-react.js')
var {vw, vh, vmin, vmax} = require('react-native-viewport-units')
var CityMapper = require('../components/CityMapper.ios.js')
var LinearGradient = require('react-native-linear-gradient')

var {
  AppRegistry,
  StyleSheet,
  Image,
  Text,
  View,
  ListView,
  MapView,
  ScrollView,
  StatusBarIOS,
  TouchableHighlight,
  LinkingIOS
} = React


var VenueScreen = React.createClass({

  _getAnnotationsArray: function() {
    return [
      {
        latitude: this.props.location._latitude,
        longitude: this.props.location._longitude,
        title: this.props.name
      }
    ]
  },

  _getRegion: function() {
    return {
      latitude: this.props.location._latitude,
      longitude: this.props.location._longitude,
      latitudeDelta: 0.0008,
      longitudeDelta: 0.0008,
    }
  },

  render: function() {
    return (
      <View>
        <View style={styles.container}>
          <View style={styles.contentContainer}>
            <Image
              style={styles.photo}
              source={{uri: this.props.image}}
            />
            <LinearGradient colors={['rgba(0,0,0,0)', '#000']} style={styles.linearGradient}>
            </LinearGradient>
            <View style={styles.textContainer}>
              <Text style={styles.name}>
                {this.props.name}
              </Text>
              <Text style={styles.distance}>
                {this.props.distance} miles away
              </Text>
            </View>
          </View>

          <View>

            <View style={styles.address}>
              <Text style={styles.addressText}>
                {this.props.address}
              </Text>
            </View>
              
            <Text style={styles.description}>
              {this.props.description}
            </Text>
          </View>
        </View>

        <MapView
          style={styles.mapView}
          region={this._getRegion()}
          annotations={this._getAnnotationsArray()}
          scrollEnabled={false}
        >
        </MapView>

        <View style={styles.otherApps}>
          <CityMapper
            startLatitude={this.props.latitude}
            startLongitude={this.props.longitude}
            endLatitude={this.props.location._latitude}
            endLongitude={this.props.location._longitude}
            endName={this.props.name}
            endAddress={this.props.address}
          />
        </View>

      </View>
    )
  }
})

var styles = StyleSheet.create({
  mapView: {
    borderColor: '#5856d6',
    borderWidth: 2,
    height: (100*vw)-40,
    marginLeft: 20,
    marginRight: 20
  },

  otherApps: {
    //backgroundColor: '#D9F0FF',
    padding: 20
  },

  button: {
    alignSelf: 'stretch',
    borderColor: '#0074D9',
    borderRadius: 3,
    borderWidth: 1,
    flexDirection: 'row',
    height: 36,
    justifyContent: 'center',
    marginTop: 20
  },
  buttonText: {
    color: '#0074D9',
    fontSize: 18,
    alignSelf: 'center'
  },

  address: {
    backgroundColor: '#5856d6',
    padding: 10
  },

  addressText: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
    paddingLeft: 10,
    paddingRight: 10
  },

  description: {
    fontSize: 16,
    lineHeight: 21,
    padding: 20,
    paddingTop: 16
  },

  distance: {
    fontFamily: 'Montserrat-Regular',
    color: '#ffffff',
    flex: 1
  },

  textContainer: {
    backgroundColor: 'transparent',
    bottom: 0,
    padding: 20,
    position: 'absolute',
    alignSelf: 'stretch'
  },

  name: {
    fontFamily: 'Montserrat-Bold',
    fontWeight: '100',
    marginBottom: 5,
    fontSize: 26,
    color: '#ffffff'
  },

  label: {
    color: '#ffffff'
  },

  text: {
    fontFamily: 'Montserrat-Regular',
    color: '#333333',
    backgroundColor: '#ffffff',
    marginTop: 8,
    marginBottom: 8
  },
  container: {
    // TODO (phunt): actually learn flexbox
    flex: 1,
    alignItems: 'stretch'
  },
  contentContainer: {
    height: 80*vw,
    width: 100*vw,
    alignItems: 'stretch'
  },
  photo: {
    flex: 1
  },
  linearGradient: {
    backgroundColor: 'transparent',
    flex: 1,
    height: (80*vw),
    width: (100*vw),
    position: 'absolute',
    top: 0
  }
});


module.exports = VenueScreen
