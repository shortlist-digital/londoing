'use strict';

var React = require('react-native')
var { Parse } = require('parse')
var ParseReact = require('parse-react/dist/parse-react.js')


var {
  AlertIOS,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  TouchableHighlight,
  Navigator
} = React

var Video = require('react-native-video')
var {vw, vh, vmin, vmax} = require('react-native-viewport-units')
var LinearGradient = require('react-native-linear-gradient')


var SearchScreen = React.createClass({

  getInitialState: function() {
    return {
      geolocating: false,
      coords: {}
    }
  },

  componentWillMount: function() {
    navigator.geolocation.watchPosition(
      this._positionSuccess,
      this._positionFailure
    )
    console.log(this.props)
  },

  componentWillReceiveProps: function(newProps) {
    console.log(this.props.name, ' new props ', newProps)
    console.log(this.props.navigator.getCurrentRoutes())
  },

  componentWillUnmount: function () {
    AlertIOS.alert('Component will unmount')
  },

  _onLocationPressed: function() {
    this.props.navigator.push({
      id: 'results',
      title: 'Results',
      passProps: { coords: this.state.coords },
      sceneConfig: Navigator.SceneConfigs.FloatFromRight
    })
  },

  _positionFailure: function (error) {
    console.log(error)
    this.setState({
      geolocating:false
    })
    AlertIOS.alert('We couldn\'t find you. You might not have GPS signal, or you may need to enable geolocation.')
  },

  _positionSuccess: function (nextPosition) {
    console.log(nextPosition)
    this.setState({
      geolocating:false,
      coords: nextPosition.coords
    })
  },


  render: function() {
    return (
      <View style={styles.container}>

        <Video source={{uri: "beers"}} 
         rate={1.0}              
         volume={1.0}
         muted={true}
         paused={false}
         resizeMode="cover"
         repeat={true}
         //onLoad={this.setDuration}
         //onProgress={this.setTime} 
         //onEnd={this.onEnd}
         style={styles.backgroundVideo} /> 

        <LinearGradient colors={['rgba(88, 86, 214,0.5)','rgba(198, 68, 252,0.5)']} style={styles.linearGradient}>
        </LinearGradient>

        <View style={styles.textContainer}>
          <Text style={styles.logo}>
            Londoing
          </Text>
          <Text style={styles.instructions}>
            What are we doing?
          </Text>
        </View>
        <TouchableHighlight
          style={styles.button}
          onPress={this._onLocationPressed}
          underlayColor='#99d9f4'
        >
          <Text style={styles.buttonText}>Search near me</Text>
        </TouchableHighlight>
      </View>
    )
  }
});

var styles = StyleSheet.create({
  linearGradient: {
    backgroundColor: 'transparent',
    flex: 1,
    height: (100*vh)-60,
    width: (100*vw),
    position: 'absolute',
    top: 0
  },
  textContainer: {
    marginTop: 20*vh,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: (100*vh)-60,
    width: 100*vw
  },

  button: {
    alignSelf: 'stretch',
    backgroundColor: 'white',
    borderColor: 'white',
    borderRadius: 0,
    borderWidth: 2,
    flexDirection: 'row',
    height: 40,
    justifyContent: 'center',
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    paddingBottom: 2
  },

  buttonText: {
    fontFamily: 'Montserrat-Bold',
    color: '#5856d6',
    fontSize: 18,
    alignSelf: 'center'
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  welcome: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },

  logo: {
    color: 'white',
    fontSize: 42,
    fontFamily: 'Montserrat-Bold'
  },

  instructions: {
    fontSize: 20,
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    color: 'white',
    marginBottom: 5,
    marginTop: 15
  },
})

module.exports = SearchScreen
