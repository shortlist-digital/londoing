'use strict';

var React = require('react-native')
var { Parse } = require('parse')
var ParseReact = require('parse-react/dist/parse-react.js')


var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  ScrollView,
  StatusBarIOS
} = React

var Venue = require('../components/Venue')

var VenueScreen = require('./VenueScreen.ios.js')

var ResultsScreen = React.createClass({

  mixins: [ParseReact.Mixin],

  observe: function (props, state) {
    console.log('running observe', props)
    var currentLocation = new Parse.GeoPoint(props.coords.latitude, props.coords.longitude)
    var venue = Parse.Object.extend("Venue")
    return {
      venues: new Parse.Query(venue).near('location', currentLocation).limit(20)
    }
  },

  componentWillMount: function () {
    console.log('Results Found')
    //StatusBarIOS.setHidden(true, StatusBarIOS.Animation['slide'])
  },

  componentWillReceiveProps: function(newProps) {
  },

  renderRow: function(row) {
    var currentLocation = new Parse.GeoPoint(this.props.coords.latitude, this.props.coords.longitude)
    var distance = row.location.milesTo(currentLocation).toFixed(2)
    return (
      <Venue
        {...row}
        {...this.props.coords}
        distance={distance}
        onVenuePress={this._selectVenue}
        />
    )
  },

  _selectVenue: function (venueProps) {
    console.log(venueProps)
    this.props.navigator.push({
      id: 'venue',
      title: 'Venue',
      passProps: {...venueProps}
    })
  },

  render: function() {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    ds = ds.cloneWithRows(this.data.venues);

    return (
      <ListView
        style={listStyles.container}
        dataSource={ds}
        renderRow={this.renderRow}
      />
    );
  }
});

var listStyles = StyleSheet.create({
  container: {
    flex: 1
  },
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 5

  },
  welcome: {
    color: '#ffffff',
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#ffffff',
    marginBottom: 5,
  },
})

module.exports = ResultsScreen
